#include <stdio.h>              
#include <math.h>
#include <stdbool.h>
#include <time.h>

#define size 1000

int getNearestDotsCount(int* dotsX, int* dotsY, int dotX, int dotY, int radius);
void generateDots(int* dots, int len);
float getUserInput(char* title);

int main(void)
{
    while (true)
    {
        srand(time(0));

        int radius = getUserInput("Enter radius: \n");
        int dotsX[size];
        int dotsY[size];
        int dotX = rand() % size - size / 2;
        int dotY = rand() % size - size / 2;
        generateDots(dotsX, size);
        generateDots(dotsY, size);
        int count = getNearestDotsCount(dotsX, dotsY, dotX, dotY, radius);
        printf("%d\n", count);
        printf("Input 'y' for continue or 'n' for exit: \n");
        char con;
        scanf_s("%c", &con, 1);
        if (con == "n")
        {
            break;
        }
    }
    return 0;
}

void generateDots(int* dots, int len)
{
    for (int i = 0; i < len; i++) 
    {
        dots[i] = rand() % size - size / 2;
    }
}

int getNearestDotsCount(int* dotsX, int* dotsY, int dotX, int dotY, int radius)
{
    int find = 0;
    for (int i = 0; i < size; i++)
    {
        if (sqrt(pow(abs(abs(dotsX[i]) - abs(dotX)), 2) + pow(abs(abs(dotsY[i]) - abs(dotY)), 2)) < radius)
            find += 1;
    }
    return find;
}

float getUserInput(char* title)
{
    printf(title);
    float p;
    if (!scanf_s("%f", &p))
    {
        return 0;
    }
    else
        return p;
}
